Debug Module for UXApp
======================

Version 1.3 -- 2023-11-25

Installation
------------

Add to your `composer.json`:

    {
      "require": {
        "uhi67/uxapp-debug": "*"
      },
    }

Usage
-----

### In the main config:

```
    'components' => [
		'debug' => if($environment == 'development') ? [
			'class' => \uhi67\debug\Debug::class,
			'dataPath' => UXApp::$app->datapath.'/debug',		// writeable path to store trace files
		] : null,
        ...
    ]
```

### In the user pages:

```
    `UXApp::trace()` calls will be served by the module  
```


Change log
==========

## Version 1.3 -- 2023-11-25

-  Support vendor dir relocation
- Debug of debug is filtered
- trace off  fixed
- Debug::d() added
-  baseurl and tab tile fixes

## Version 1.2 -- 2021-03-24

- bugfixes,
- using fortawesome/font-awesome, components/jquery
- uxapp/1.5 compatibility

## Version 1.1 -- Release 2020-11-27

- using uxapp 1.1 (UrlFormatter, AssetManager)
- dataPath (default is menu)
- display stuctured data in trace pane
- font & other style changes
- Debug::p() displays place
- XML switch on control pane

## Version 1.0 -- Release 2020-11-03

- first public release