<?xml version="1.0" encoding="UTF-8"?>
<!--suppress XmlDefaultAttributeValue, JSUnresolvedLibraryURL -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml">
    <xsl:output method="xml" version="1.0" indent="yes"
                doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
                doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
                omit-xml-declaration="yes"
    />

    <xsl:template match="dummy" mode="syntax-tree" />

    <xsl:template match="/data/head" mode="user-head">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous" />
        <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@400;700&amp;family=Roboto+Mono:wght@400;700&amp;family=Roboto+Slab:wght@400;700&amp;display=swap" rel="stylesheet"/>
    </xsl:template>

    <xsl:template match="data">
        <xsl:apply-templates select="content" />
    </xsl:template>

    <xsl:template match="content">
        <nav class="navbar navbar-expand navbar-dark bg-dark debug-trace">
            <a class="navbar-brand">Trace</a>
            <form class="form-inline">
                <input type="hidden" name="session" value="{/data/content/debug}" />
                <select id="debug-request" name="request" class="form-control submit">
                    <xsl:for-each select="requests/request">
                        <xsl:if test="not(contains(@name, 'GET /module/uhi67/uxapp-debug'))">
                            <option id="request_{@id}" value="{@id}">
                                <xsl:if test="@current=1"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
                                <xsl:value-of select="@name"/>
                            </option>
                        </xsl:if>
                    </xsl:for-each>
                </select>
            </form>
        </nav>

        <xsl:apply-templates select="tabcontrol" />
    </xsl:template>

    <xsl:template match="tab[@id='request']" mode="user-content">
        <xsl:apply-templates select="/data/content/log/item[tags='request']"/>
    </xsl:template>

    <xsl:key name="tag" match="/data/content/log/item/tags" use="." />

    <xsl:template match="tab[@id='all']" mode="user-content">
        <div class="tags">
            <div class="title">Tags:</div>
            <xsl:for-each select="/data/content/log/item/tags[generate-id()=generate-id(key('tag',.)[1])]">
                <xsl:if test=".!='uxpage-xmldoc' and .!='request'">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input tag-switch" type="checkbox" id="tag-{.}" value="{.}" checked="checked" />
                        <label class="form-check-label" for="tag-{.}"><xsl:value-of select="."/></label>
                    </div>
                </xsl:if>
            </xsl:for-each>
        </div>
        <xsl:apply-templates select="/data/content/log/item[not(tags='request') and not(tags='uxpage-xmldoc')]"/>
    </xsl:template>

    <xsl:template match="tab[@id='app']" mode="user-content">
        <xsl:apply-templates select="/data/content/log/item[tags='app']"/>
    </xsl:template>

    <xsl:template match="tab[@id='sql']" mode="user-content">
        <xsl:apply-templates select="/data/content/log/item[tags='sql']"/>
    </xsl:template>

    <xsl:template match="tab[@id='xml']" mode="user-content">
        <h3>XML output</h3>
        <xsl:apply-templates select="/data/content/log/item[tags='uxpage-xmldoc']/xml" mode="syntax-tree"/>
    </xsl:template>

    <xsl:template match="item">
        <xsl:variable name="tags">
            <xsl:text>|</xsl:text>
            <xsl:for-each select="tags"><xsl:value-of select="."/>|</xsl:for-each>
        </xsl:variable>
        <div class="item-container" data-tags="{$tags}">
            <div class="item-where">
                <i class="fa fa-plus-square"/>
                <span class="ts"><xsl:value-of select="@ts"/></span>
                <span class="file"><xsl:apply-templates select="@file" mode="filename"/></span>
            </div>
            <dl class="item-list hidden">
                <xsl:for-each select="backtrace">
                    <dt class="backtrace"><xsl:value-of select="@file"/>#<xsl:value-of select="@line"/></dt>
                    <dd><xsl:value-of select="@class"/><xsl:value-of select="@type"/><xsl:value-of select="@function"/><xsl:value-of select="@args"/></dd>
                </xsl:for-each>
            </dl>
            <div class="item level-{@level}">
                <i class="fa fa-plus-square"/>
                <span class="msg">
                    <xsl:value-of select="@message"/>
                    <xsl:if test="message[1]">
                        <xsl:text>[</xsl:text>
                        <xsl:for-each select="message">
                            <xsl:if test="string(number(@index)) != @index">
                                <xsl:value-of select="@index" />=
                            </xsl:if>
                            <xsl:for-each select="text()">
                                <span class="text">
                                    <xsl:value-of select="."/>
                                </span>
                            </xsl:for-each>
                            <xsl:apply-templates select="*" mode="syntax-tree" />
                            <xsl:text>, </xsl:text>
                        </xsl:for-each>
                        <xsl:text>]</xsl:text>
                    </xsl:if>
                </span>
                <span class="sum">
                    <xsl:text> (</xsl:text><xsl:value-of select="count(data)"/><xsl:text>)</xsl:text>
                </span>
            </div>
            <dl class="item-list hidden">
                <xsl:for-each select="@*">
                    <xsl:if test="substring(name(),1,1)!='_' and name()!='level' and name()!='message'">
                        <dt class="attr"><xsl:value-of select="name()"/></dt>
                        <dd><xsl:value-of select="."/></dd>
                    </xsl:if>
                </xsl:for-each>
                <xsl:for-each select="data">
                    <dt class="data"><xsl:value-of select="@index"/></dt>
                    <dd>
                        <dl class="sub">
                            <xsl:apply-templates select="text()" mode="data" />
                            <xsl:apply-templates select="*" mode="data" />
                        </dl>
                    </dd>
                </xsl:for-each>
            </dl>
        </div>
    </xsl:template>

    <xsl:template match="filename">
        <xsl:choose>
            <xsl:when test="starts-with(., '/vendor/uhi67/uxapp')">
                <span class="uxapp">uxapp</span>
                <xsl:value-of select="substring-after(., '/vendor/uhi67/uxapp')" />
            </xsl:when>
            <xsl:when test="starts-with(., '/vendor/uhi67/uxapp-')">
                <span class="uxapp-module">uxapp-<xsl:value-of select="substring-before(substring-after(., '/vendor/uhi67/uxapp-'), '/')" /></span>
                <xsl:text>/</xsl:text>
                <xsl:value-of select="substring-after(substring-after(., '/vendor/uhi67/uxapp-'), '/')" />
            </xsl:when>
            <xsl:when test="starts-with(., '/vendor/')">
                <xsl:variable name="vmf" select="substring-after(., '/vendor/')"/>
                <span class="vendor-module">
                    <xsl:value-of select="substring-before($vmf, '/')" />
                </span>
                <xsl:value-of select="substring-after(., '/vendor/')" />
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="text()" mode="data">
        <div class="string"><xsl:value-of select="." /></div>
    </xsl:template>

    <xsl:template match="*[@index=0]" mode="data">
        <xsl:variable name="name" select="name()" />
        <dt><xsl:value-of select="name()" /></dt>
        <dd>
            <dl class="array">
                <xsl:for-each select="../*[name()=$name]">
                    <dt><xsl:value-of select="@index" /></dt>
                    <dd>
                        <dl class="sub">
                            <xsl:apply-templates select="text()" mode="data" />
                            <xsl:apply-templates select="*" mode="data" />
                        </dl>
                    </dd>
                </xsl:for-each>
            </dl>
        </dd>
    </xsl:template>

    <xsl:template match="*[@index>0]" mode="data">
    </xsl:template>

    <xsl:template match="*[not(@index)]" mode="data">
        <dt><xsl:value-of select="name()" /></dt>
        <dd class="single">
            <dl class="sub">
                <xsl:apply-templates select="text()" mode="data" />
                <xsl:apply-templates select="*" mode="data" />
            </dl>
        </dd>
    </xsl:template>

    <xsl:template match="@*" mode="filename">
        <!-- Display filename with module name -->
        <xsl:choose>
            <xsl:when test="contains(., 'vendor/uhi67/uxapp-')">
                <span class="module-name-uxapp"><xsl:value-of select="substring-before(substring-after(., 'vendor/uhi67/'), '/')"/></span>:
                <span class="file-end"><xsl:value-of select="substring-after(substring-after(., 'vendor/uhi67/'), '/')"/></span>
            </xsl:when>
            <xsl:when test="contains(., 'vendor/')">
                <span class="module-name"><xsl:value-of select="substring-before(substring-after(., 'vendor/'), '/')"/></span>:
                <span class="file-end"><xsl:value-of select="substring-after(substring-after(., 'vendor/'), '/')"/></span>
            </xsl:when>
            <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
