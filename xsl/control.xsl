<?xml version="1.0" encoding="UTF-8"?>
<!--suppress XmlDefaultAttributeValue -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="xml" version="1.0" indent="yes"
				doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
				doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
				omit-xml-declaration="yes"
	/>

	<xsl:template match="data">
		<xsl:apply-templates select="content" />
	</xsl:template>

	<xsl:template match="content">
		<div class="control">
			<form action="$url" id="control-form" onsubmit="return go_new()">
				<input type="hidden" name="debug" value="control" />
				<input type="hidden" name="session" value="{debug}" />
				<input type="hidden" name="lasturl" id="lasturl" value="{url}" />
				<input type="hidden" name="baseurl" id="baseurl" value="{baseurl}" />
				<input type="hidden" name="baseurl" id="baseurl" value="{showxml}" />

				<b>Debugging</b> <input id="control-url" name="url" value="{url}" style="width:30%" />

				<div class="button" id="control-go">GO</div>
				<a class="button danger" href="{url_off}" target="_top">Debug OFF</a>
				<a class="button warning" href="{url_reset}" target="_top">Reset log</a>
				<div class="button success" id="control-refreshtrace">Refresh trace</div>
				<div class="button" id="control-refreshmain">Refresh main</div>
				<xsl:choose>
					<xsl:when test="showxml=1"><div class="button" id="control-xmloff">Hide XML</div></xsl:when>
					<xsl:otherwise><div class="button" id="control-xmlon">Show XML</div></xsl:otherwise>
				</xsl:choose>
				<input id="control-session" readonly="readonly" name="session" value="{debug}" style="width:175px" />
			</form>
		</div>
	</xsl:template>

</xsl:stylesheet>
