﻿<?xml version="1.0" encoding="UTF-8"?>
<!--suppress XmlUnusedNamespaceDeclaration -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:md="urn:oasis:names:tc:SAML:2.0:metadata"
	xmlns:mdui="urn:oasis:names:tc:SAML:metadata:ui"
    xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="xml" version="1.0" indent="yes"
		doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
		doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
		omit-xml-declaration="yes"
	 />

	<xsl:template match="*" mode="syntax-tree">
		<xsl:param name="state" select="'open'" /> <!-- Specify 'closed' for closed initial state-->
		<div class="syntax-tree">
			<xsl:call-template name="syntax">
				<xsl:with-param name="state" select="$state" />
			</xsl:call-template>
		</div>
	</xsl:template>

	<xsl:template name="syntax" >
		<xsl:param name="state" select="'open'" /> <!-- Specify 'closed' for closed initial state-->
		<xsl:choose>
			<xsl:when test="name()=''"><xsl:value-of select="."/></xsl:when>
			<xsl:otherwise>
				<span class="tag">
					<span class="open-tag">
						<xsl:if test="count(*)>0 or text()">
							<xsl:choose>
								<xsl:when test="$state='closed'">
									<i class="fa far fa-plus-square">&#160;</i>
								</xsl:when>
								<xsl:otherwise>
									<i class="fa far fa-minus-square">&#160;</i>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:if>
						<xsl:if test="count(*)=0 and not(text())"><i class="fa far fa-square">&#160;</i></xsl:if>
						<xsl:text>&lt;</xsl:text>
						<b><xsl:value-of select="name()" /></b>
					</span>

					<!-- namespace -->
					<xsl:if test="namespace-uri()!=namespace-uri(..)">
						<xsl:text> </xsl:text>
						xmlns="<i style="color:#93c"><xsl:value-of select="namespace-uri()" /></i>"
						<xsl:text> </xsl:text>
					</xsl:if>

					<xsl:for-each select="@*" >
						<xsl:text> </xsl:text>
						<span class="attr-value">
							<span class="attr"><xsl:value-of select="name()" /></span>="<span class="value"><xsl:value-of select="." /></span><xsl:text>"</xsl:text>
						</span>
					</xsl:for-each>
					<xsl:if test="count(*)=0 and count(text())=0">
						<xsl:text> /&gt;</xsl:text><br />
					</xsl:if>
					<xsl:if test="count(*)=0 and count(text())>0">
						<xsl:text>&gt;</xsl:text>
						<xsl:for-each select="text()">
							<span class="text">
								<xsl:value-of select="."/>
							</span>
						</xsl:for-each>
						<span class="shorten">...</span>
						<span class="close-tag">
							<xsl:text>&lt;/</xsl:text>
							<xsl:value-of select="name()" />
							<xsl:text>&gt;</xsl:text><br />
						</span>
					</xsl:if>
					<xsl:if test="count(*)>0">
						<xsl:text>&gt;</xsl:text>
						<xsl:variable name="hidden"><xsl:if test="$state='open'">hidden</xsl:if></xsl:variable>
						<span class="sum {$hidden}">
							<xsl:text>&#160;(</xsl:text>
							<xsl:value-of select="count(.//*)" />
							<xsl:text>)</xsl:text>
						</span>
					</xsl:if>
				</span>
				<xsl:if test="count(*)>0">
					<dl class="content">
						<xsl:if test="$state='closed'"><xsl:attribute name="class">hidden</xsl:attribute></xsl:if>
						<dd>
							<xsl:for-each select="node()" >
								<xsl:call-template name="syntax" />
							</xsl:for-each>
						</dd>
					</dl>
					<span class="close-tag">
						<xsl:text>&lt;/</xsl:text>
						<xsl:value-of select="name()" />
						<xsl:text>&gt;</xsl:text><br />
					</span>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
