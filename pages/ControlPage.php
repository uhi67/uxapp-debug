<?php /** @noinspection PhpIllegalPsrClassPathInspection */

namespace uhi67\debug\pages;

use Exception;
use ReflectionException;
use uhi67\uxapp\exception\HandledException;
use uhi67\uxapp\Util;
use uhi67\uxapp\UXApp;
use uhi67\uxapp\UXAppException;
use uhi67\uxapp\UXAppPage;

class ControlPage extends UXAppPage {
    /**
     * @throws ReflectionException
     * @throws UXAppException
     * @throws HandledException
     * @throws Exception
     */
    public function prepare() {
        $this->noFlash = true;
        $this->app->debug->off();
        parent::prepare();
        $this->setLayout('uhi67/uxapp:simple');
        $this->view->addXsl(['uhi67/uxapp', 'copy.inc.xsl']);

		$faAsset = $this->app->assetManager->register([
			'dir' => 'fortawesome/font-awesome',
			'patterns' => [
				'css/*.css',
				'webfonts/*',
			],
		]);
		$faAsset->register($this->view, ['css/all.min.css']);

		$uXAppDebugAsset = $this->app->assetManager->register([
			'dir' => 'uhi67/uxapp-debug',
			'patterns' => ['www/js/debug.js', 'www/css/debug.css'],
		]);
		$uXAppDebugAsset->register($this->view, ['www/js/debug.js', 'www/css/debug.css']);

    }

    /**
     * @throws Exception
     */
    public function actDefault() {
        $this->setView('uhi67/uxapp-debug:control');
        $session = $this->request->get('session', $this->session->id);
        $url = $this->request->get('debug-url');

        $urlFormatter = UXApp::$app->urlFormatter;
        $url1 = $urlFormatter->modUrl(Util::substring_before($url, '#', true), ['debug-session'=>null]);

        $this->view->addContentNode('debug', $session);
        $this->view->addContentNode('url', $url);
        $this->view->addContentNode('baseurl', $this->request->baseUrl);
        $this->view->addContentNode('url_off', $urlFormatter->modUrl($url1, ['trace'=>'off']));
        $this->view->addContentNode('url_left', $urlFormatter->modUrl($url1, ['debug'=>'left', 'url'=>$url]));
        $this->view->addContentNode('url_reset', $urlFormatter->modUrl($url1, ['trace'=>'reset']));
        $this->view->addContentNode('showxml', $this->session->get('showxml'));
        return true;
    }
}
