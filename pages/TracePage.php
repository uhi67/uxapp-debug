<?php /** @noinspection PhpIllegalPsrClassPathInspection */

namespace uhi67\debug\pages;

use Exception;
use ReflectionException;
use uhi67\tabcontrol\TabControl;
use uhi67\uxapp\ArrayUtils;
use uhi67\uxapp\exception\HandledException;
use uhi67\uxapp\UXAppException;
use uhi67\uxapp\UXAppPage;

class TracePage extends UXAppPage {

    /**
     * @throws ReflectionException
     * @throws UXAppException
     * @throws HandledException
     * @throws Exception
     */
    public function prepare() {
        $this->noFlash = true;
        parent::prepare();
        $this->setLayout('uhi67/uxapp:simple'); // Override (invalidate) applications's design
        $this->view->addXsl(['uhi67/uxapp-debug', 'copy.xsl']);

		$faAsset = $this->app->assetManager->register([
			'dir' => 'fortawesome/font-awesome',
			'patterns' => [
				'css/*.css',
				'webfonts/*',
			],
		]);
		$faAsset->register($this->view, ['css/all.min.css']);

		$uXAppDebugAsset = $this->app->assetManager->register([
			'dir' => 'uhi67/uxapp-debug',
			'patterns' => ['www/js/debug.js', 'www/css/debug.css'],
		]);
		$uXAppDebugAsset->register($this->view, ['www/js/debug.js', 'www/css/debug.css']);
	}

    /**
     * Request
     * @throws Exception
     */
    public function actDefault() {
        $this->setView('uhi67/uxapp-debug:trace');
        $session = $this->request->get('session', $this->session->id);
        $url = $this->request->get('url');
        $request = $this->request->getInt('request');

        $this->view->addContentNode('debug', $session);
        $this->view->addContentNode('url', $url);

        $tf = $this->app->debug->dataPath.'/'.$session.'.log';

        $node_requests = $this->view->addContentNode('requests');
        $node_log = $this->view->addContentNode('log');

        if(file_exists($tf)) {
            //$trace = file_get_contents($tf); $this->view->addContentNode('trace', $trace); // test only

            // Determine last request
            if(!$request) {
                $f = fopen($tf, 'r');
                $last = 0;
                $node_last = null;
                while(!feof($f)) {
                    $l = fgets($f);
                    if(strrpos($l, '"message":"SERVER"')) {
                        $last++;
                    }
                }
                fclose($f);
                $request = $last;
            }

            // Parse requests
            $rr = [];
            $f = fopen($tf, 'r');
            $id = 0;
            $node_last = null;
            while(!feof($f)) {
                $l = fgets($f);
                $t = json_decode($l, true);
                if($t === null) $rr[] = null;
                else {
                    $message = ArrayUtils::getValue($t, 'message');
                    if($message == 'SERVER') {
                        $data = ArrayUtils::getValue($t, 'data');
                        $time = date('Y-m-d H:i:s', $data['REQUEST_TIME']);
                        $query = []; parse_str($data['QUERY_STRING'], $query);
                        unset($query['debug-session']);
                        $name = $data['REQUEST_METHOD'].' '.$data['REQUEST_URI'];
                        $node_last = $node_requests->addNode('request', [
                            '_insert_before' => $node_last,
                            'id' => ++$id,
                            'name' => $time . ' ' . $name,
                            'current' => $id==$request ? 1 : null,
                        ]);
                    }
                    if($id == $request) {
                        $filter = ['depth', 'ip', 'timestamp'];
                        $xml = null;
                        if(is_string($message) && substr($message,0,6)=='<?xml ') {
                            $xml = $message;
                            $t['message'] = 'xmldoc';
                        }
                        // TODO: ez nem jó, rosszul képezi le a tömböket.
                        $node_item = $node_log->addNode('item', array_diff_key($t, array_flip($filter)));
                        if($xml) {
                            $node_item->addHypertextNode('xml', $xml);
                        }
                    }
                }
            }
            fclose($f);
            if(!$request && $node_last) $node_last->setAttribute('current', 1);

            ($tabcontrol = new TabControl([
                'page' => $this,
                'name' => 'debug-trace-tab',
                'pages' => [
                    'request' => ['Request', 'Request headers and parameters'],
                    'all' => ['All', 'All log items'],
                    'app' => ['App', 'Application log items'],
                    'sql' => ['SQL', 'SQL commands'],
                    'xml' => ['XML', 'The XML output document'],
                ],
                'default' => 0,
                'instant' => true
            ]))->createNode();
        }
        else {
            $this->addMessages("Trace file `$tf` not found.", ['class'=>'error']);
        }
        return true;
    }
}
