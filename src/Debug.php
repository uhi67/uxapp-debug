<?php /** @noinspection PhpIllegalPsrClassPathInspection */
/** @noinspection PhpUnused */
/** @noinspection PhpMethodNamingConventionInspection */

namespace uhi67\debug;
use Exception;
use uhi67\uxapp\ArrayUtils;
use uhi67\uxapp\BaseDebugger;
use uhi67\uxapp\BaseModel;
use uhi67\uxapp\DebugInterface;
use uhi67\uxapp\Util;
use uhi67\uxapp\UXApp;
use uhi67\uxapp\UXAppException;
use uhi67\uxapp\UXAppLogger;

/**
 * # Debug module for UXApp framework (web user interface only)
 *
 * Implements DebugInterface
 *
 * ### Configuration parameters
 *
 * - enabled
 * - dataPath -- location of logfiles (default is app->dataPath/debug)
 *
 * ### Public methods
 *
 * - isOn()
 * - getId()
 * - trace()
 *
 * ### Usage
 *
 * 1. Register in main config as a logger
 * 2. user may swith on (or off) debugger with trace=on (or off) request parameter
 * 3. Debugger will display a frameset with debugger panes
 * 4. Any log event, including UXApp::debug, will be logged and displayed in debugger
 *
 * ### Used request parameters
 *
 * - trace: on=starts trace (displays frameset); off=switch off logging
 *
 * See {@see BaseDebugger}
 *
 * @property string $rootPath
 * @property string $dataPath -- location of logfiles (default is app->dataPath/debug)
 * @property-read string $traceFile -- actual filename (full path) of the current trace logfile, based on session
 */
class Debug extends BaseDebugger implements DebugInterface {
    // Configuration properties

    /** @var float $ts -- debug start microtime on the current request */
    public $ts;

    /**
     * @throws Exception
     */
    public function prepare() {
        parent::prepare();

        if(!$this->dataPath && $this->parent) $this->dataPath = $this->parent->dataPath.'/debug';
        if(!$this->dataPath) throw new Exception("Invalid dataPath for Debug module");
        if(!file_exists($this->dataPath)) {
            if(!mkdir($this->dataPath, 0774)) {
                throw new Exception("Failed to create dir `$this->dataPath`");
            }
        }
        $this->ts = microtime(true);
    }

    /**
     * @throws UXAppException
     */
    public function init() {
        parent::init();

        $trace = $this->parent->request->req('trace');
        if($trace=='on') {
            $this->start();
            exit;
        }
        elseif($trace=='off') {
            $this->disable();
        }
        elseif($trace=='reset') {
            $this->on();
            if(file_exists($this->traceFile)) @unlink($this->traceFile);
            $this->start();
        }
        if($this->parent->request->get('module')=='uhi67/uxapp-debug') $this->off();
        if($this->isOn()) {
            ini_set('display_errors', 'stdout');
            $this->trace_start();
        }
    }

    /**
     * Returns current trace id
     *
     * @return string
     */
    public function getId(): string {
        return $this->enabled ? $this->on : '';
    }

    /**
     * Starts debugging. Displays frameset
     *
     * @throws UXAppException
     */
    public function start() {
        $this->enable();
        $this->on();
        $url = UXApp::$app->urlFormatter->modUrl(null, ['debug-session'=>null, 'trace'=>null]);
        $this->frame($url);
    }

    /**
     * Logs request main data
     */
    public function trace_start() {
        if(!$this->isOn()) return;
        $this->enable();

        $this->trace_server();
        $this->trace_headers();
        $this->trace_query();
        $this->trace_post();
        $this->trace_files();
        $this->trace_session();
        $this->trace_cookie();
        $this->trace_saml();
    }

    /**
     * Logs a data record into session tracefile
     *
     * Debug-related context parameters:
     *
     * - depth (effective backtrace starting depth from trace call)
     * - tags (space spearated words, e.g. 'app', 'uxapp', 'sql') to filter results
     * - mt (microtime to display)
     * - color (message color)
     *
     * Log file format: one line/record, json object, context complemented with:
     * - level
     * - message
     *
     * see also {@see UXAppLogger::log()}
     *
     * @param string $level -- [emergency, alert, critical, error, warning, notice, info, debug]
     * @param mixed $message
     * @param array $context
     *
     * @return void|null
     * @throws UXAppException
     */
    public function log($level, $message, array $context = []) {
        if(!$this->isOn()) return;

        $context['depth'] = $depth = ArrayUtils::getValue($context, 'depth', 3);
        if(!isset($context['tags'])) $context['tags'] = 'app';
        if(is_string($tags = $context['tags'])) $tags = $context['tags'] = explode(' ', $tags);

        if(ArrayUtils::getValue($context, '_log_location')!=='no') {
            $da = debug_backtrace();
            // determines depth if sql tag used
            if(in_array('sql', $tags)) {
                for($i = $depth; $i < count($da); $i++) {
                    $dx = $da[$i];
                    if(!preg_match('#uxapp[/\\\\]lib[/\\\\](DBX|Query)[a-z]*.php#i', ArrayUtils::getValue($dx, 'file', '$?'))) {
                        $depth = $i + 1;
                        break;
                    }
                }
            }

            // Determine call location
            $dd = $da[$depth] ?? [];
            $context['where'] = ArrayUtils::getValue($dd, 'class', '') . ArrayUtils::getValue($dd, 'type', '') . ArrayUtils::getValue($dd, 'function', '');
            $dad1 = $da[$depth - 1];
            $context['file'] = $this->parent->relativePath(ArrayUtils::getValue($dad1, 'file', '$?') . '#' . ArrayUtils::getValue($dad1, 'line', '#?'));
        }

        $context['ts'] = sprintf('%.3f', microtime(true) - $this->ts);
        $context['message'] = is_string($message) ? UXAppLogger::interpolate($message, $context) : Util::objtostr($message);

        if(ArrayUtils::fetchValue($context, '_log_backtrace')!=='no') {
            $context['backtrace'] = Util::getBacktrace($depth, $depth+4, 2);
        }

        $fp = fopen($this->traceFile, 'a');
        if(!$fp) return null;
        fwrite($fp, json_encode($context)."\n");
        fclose($fp);
    }

    public function trace_headers() {
        $data = getallheaders();
        $this->debug('HEADERS', ['data'=> $data, '_log_backtrace'=>'no', 'tags'=>'request']);
    }

    public function trace_server() {
        $server = $_SERVER;
        unset($server['OPENSSL_CONF']);
        unset($server['PATH']);
        unset($server['HTTP_UPGRADE_INSECURE_REQUESTS']);
        $server['REQUEST_URI'] = UXApp::$app->urlFormatter->modUrl(ArrayUtils::getValue($server, 'REQUEST_URI'), ['debug-session'=>null]);
        //unset($server['COMSPEC']);
        //unset($server['PATHEXT']);
        //unset($server['']);
        $this->debug('SERVER', ['data'=> $server, '_log_backtrace'=>'no', 'tags'=>'request']);
    }

    public function trace_query() {
        $this->debug('REQUEST', ['data' => $_REQUEST, '_log_backtrace'=>'no', 'tags'=>'request']);
    }

    public function trace_post() {
        $this->debug('POST', ['data' => $_POST, '_log_backtrace'=>'no', 'tags'=>'request']);
    }

    public function trace_files() {
        $this->debug('FILES', ['data' => $_FILES, '_log_backtrace'=>'no', 'tags'=>'request']);
    }

    public function trace_session() {
        $this->debug('SESSION', ['data' => $_SESSION, '_log_backtrace'=>'no', 'tags'=>'request']);
    }

    public function trace_cookie() {
        $this->debug('COOKIE', ['data' => $_COOKIE, '_log_backtrace'=>'no', 'tags'=>'request']);
    }

    private function trace_saml() {
        if($this->isEnabled()) {
            if(!isset($_SESSION['SimpleSAMLphp_SESSION'])) return;
            $saml = $_SESSION['SimpleSAMLphp_SESSION'];
            $obj = unserialize($saml);
            $this->debug('SAML', ['data' => $obj, '_log_backtrace'=>'no', 'tags'=>'request']);
        }
    }

    /**
     * Logs a full backtrace
     *
     * @param int $depth
     * @param int $limit
     *
     * @return void
     */
    public function backtrace($depth=0, $limit=0) {
        $this->debug('Backtrace', ['backtrace'=>Util::getBacktrace($depth, $limit)]);
    }

    /**
     * Replaces all values to string, structured values to typename only
     *
     * @param array $a
     * @return array
     */
    static function simpleValues(array $a): array {
        array_walk($a, function(&$v,	/** @noinspection PhpUnusedParameterInspection */ $i=0) {
            $t = isset($v) ? gettype($v) : 'unset';
            switch($t) {
                case 'integer':
                case 'double':
                case 'string':
                case 'boolean': $v=(string)$v; break;
                default: $v=$t;
            }
        });
        return $a;
    }

    /**
     * Outputs main frameset containing debug, control and application frames
     *
     * @param string $url -- the original url
     *
     * @return void
     * @throws UXAppException
     */
    public function frame(string $url) {
        // Cache tiltás
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
        header("Cache-Control: no-cache, must-revalidate");  // HTTP/1.1
        header("Pragma: no-cache");                          // HTTP/1.0
        header("Content-type: text/html; charset=UTF-8");

        $session = $this->parent->session->id;

        $url_control = $this->urlFormatter->createUrl(['module'=>$this->moduleId, 'page'=>'control', 'session'=>$session, 'debug-url'=>$url]);
        $url_trace = $this->urlFormatter->createUrl(['module'=>$this->moduleId, 'page'=>'trace', 'session'=>$session]);
        $url_on = UXApp::$app->urlFormatter->modUrl($url, ['debug-session'=>$session]);

        $base_url = $this->parent->createUrl([]);

        echo /** @lang text */ <<<EOT
        <?xml version="1.0"?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" lang="hu" xml:lang="hu">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>debug - $url</title>
            <base href="$base_url" />
        </head>
        <!-- frames -->
        <frameset cols="300, *">
            <frame id="left" name="trace" src="$url_trace" marginwidth="4" marginheight="4" scrolling="auto" frameborder="1"/>
            <frameset  rows="100, *">
                <frame id="control" name="control" src="$url_control" marginwidth="10" marginheight="10" scrolling="auto" frameborder="1" />
                <frame id="main" name="main" src="$url_on" marginwidth="10" marginheight="10" scrolling="auto" frameborder="1" />
            </frameset>
        </frameset>
        </html>
EOT;
    }

    /**
     * Converts object to displayable dl-indented html string
     * Uses depth limit
     *
     * @param mixed $obj
     * @param integer $depth -- depth limit, default 10.
     * @param string $name
     *
     * @return string
     */
    static function objtostr($obj, int $depth=10, string $name='array'): string {
        if(php_sapi_name() == "cli") return Util::objtostr($obj,false);
        if($depth<1) return '...';
        if(is_null($obj)) return '<span>null</span>';
        if(is_bool($obj)) return '<span>'.($obj ? 'true': 'false').'</span>';
        if(is_int($obj)) return '<span>'.$obj.'</span>';
        if(is_float($obj)) return '<span>'.$obj.'</span>';
        if(is_string($obj)) return "<span>'".htmlspecialchars($obj)."'</span>";
        if(is_array($obj)) {
            $result = '<dl class="d_obj">';
            foreach( $obj as $key => $item ) {
                //if($result!='') $result.=', ';
                $result .= '<dt>'.htmlspecialchars($key).'</dt><dd>'.self::objtostr($item, $depth-1).'<dd>';
            }
            $result .= '</dl>';
            return '<span class="d_sw"><i>'.$name.'</i></span>'.$result;
        }
        if(is_object($obj)) {
            if($obj instanceof BaseModel) {
                try {
                    return static::objtostr($obj->toArray(), $depth - 1, get_class($obj));
                }
                catch(Exception $e) {
                    return get_class($obj).'(error)';
                }
            }
            $result = '';
            foreach( $obj as $key => $item ) {
                if($result!='') $result.=',';
                $result .= $key.'='.self::objtostr($item, $depth-1);
            }
            return get_class($obj).'{'.$result.'}';
        }
        return $obj;
    }

    /**
     * Instant debug to output
     *
     * @param mixed ...$any
     * @return void
     */
    static function p(...$any) {
        $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        $bte = $backtrace[0];

        $bt2 = $backtrace[1];
        $obj = isset($bt2['class']) ? $bt2['class'] . $bt2['type'] : '';
        $title = sprintf('%s #%d %s%s', $bte['file'], $bte['line'], $obj, $bt2['function']);

        echo "<div class='dump'><h2>$title</h2><pre>";
        foreach($any as $value) {
            echo Util::objtostr($value), "\n";
        }
        echo "</pre></div>";
    }

    /**
     * Returns a html-formatted backtrace
     *
     * @param int $depth -- relative top from here
     * @param int $limit -- displayed stack frames counted from the depth
     *
     * @return string
     */
    public static function show_backtrace(int $depth=3, int $limit=0): string {
        $rootpath = UXApp::$app ? UXApp::$app->rootPath : dirname(__DIR__, 4);
        if($depth<0 or $depth>20) $depth = 3;
        $bt = debug_backtrace(1, $limit ? $depth + $limit : 0);
        while($depth>0) { array_shift($bt); $depth--; }
        return implode("\n", array_map(function($f, $index) use($rootpath) {
            return sprintf($index==0 ? '#%d %s(%d)  ' : '#%d %s(%d): %s%s%s(%s)  ',
                $index,
                str_replace($rootpath, '...', ArrayUtils::getValue($f, 'file', '')),
                ArrayUtils::getValue($f, 'line', ''),
                ArrayUtils::getValue($f, 'class', ''),
                ArrayUtils::getValue($f, 'type', ''),
                $f['function'],
                implode(', ', array_map(function($a) { return Util::objtostr($a, true, true); }, $f['args']))
            );
        }, $bt, array_keys($bt)));
    }

    /**
     * Quick dumps all arguments (varible number) with backtrace and exits.
     *
     * @noinspection PhpUnused
     */
    public static function dump() {
        echo '<h2>Quick dump</h2><pre>';
        for($i=0; $i<func_num_args(); $i++) var_dump(func_get_arg($i));
        echo "\n";
        echo static::show_backtrace(1);
        exit;
    }

    public static function d() {
        echo '<pre style="font-size:10px">';
        for($i=0; $i<func_num_args(); $i++) var_dump(func_get_arg($i));
        echo "</pre>\n";
        $d = debug_backtrace();
        printf('<div style="font-size:11px;color:red;">%s #%d</div>', $d[0]['file'], $d[0]['line']);
    }

    public function getTraceFile(): string {
        return $this->dataPath . '/' . $this->on . '.log';
    }
}
