/*
	debug.js
	run only in trace and control frames
*/

const trace_wait = 2000;

Array.prototype.intersection = function(array1, array2) {
	if(array2===undefined) array2 = this;
	return array1.filter(function(n) {
		return array2.indexOf(n) !== -1;
	})
};

Array.prototype.hasIntersection = function(array1, array2) {
	if(array2===undefined) array2 = this;
	for(const e of array1) {
		if(array2.indexOf(e) !== -1) return true;
	}
	return false;
};

(function($){
	$(function() {

		// Trace frame functions

		$('span.d_sw').on('click', function(){
			$(this).next().toggle();
			$(this).toggleClass('minus');
		});

		$('div#tabcontrol span.head').click(function() {
			$('div#head').removeClass('hidden');
			$('div#main').addClass('hidden');
			$('div#output').addClass('hidden');
			$('span.tab', $(this).parent()).removeClass('active');
			$(this).addClass('active');
		});
		$('div#tabcontrol span.main').click(function() {
			$('div#head').addClass('hidden');
			$('div#main').removeClass('hidden');
			$('div#output').addClass('hidden');
			$('span.tab', $(this).parent()).removeClass('active');
			$(this).addClass('active');
			// show all tags
			$('div.dd', 'div#main').removeClass('hidden');
		});
		$('div#tabcontrol span.app').click(function() {
			$('div#head').addClass('hidden');
			$('div#main').removeClass('hidden');
			$('div#output').addClass('hidden');
			$('span.tab', $(this).parent()).removeClass('active');
			$(this).addClass('active');
			// hide SQL tags
			$('div.dd[data-tags~="app"]', 'div#main').removeClass('hidden');
			$('div.dd[data-tags~="sql"]', 'div#main').addClass('hidden');
		});
		$('div#tabcontrol span.sql').click(function() {
			$('div#head').addClass('hidden');
			$('div#main').removeClass('hidden');
			$('div#output').addClass('hidden');
			$('span.tab', $(this).parent()).removeClass('active');
			$(this).addClass('active');
			// show SQL tags only (and not UApp)
			$('div.dd', 'div#main').addClass('hidden');
			$('div.dd[data-tags~="sql"]', 'div#main').removeClass('hidden');
			$('div.dd[data-tags~="uapp"]', 'div#main').addClass('hidden');

		});
		$('div#tabcontrol span.output').click(function() {
			$('div#head').addClass('hidden');
			$('div#main').addClass('hidden');
			$('div#output').removeClass('hidden');
			$('span.tab').removeClass('active');
			$(this).addClass('active');
		});

		$('span.tag i.fa').click(function() {
			let $tag;
			if($(this).hasClass('fa-minus-square')) {
				$tag = $(this).parent().parent();
				$(this).parent().nextAll('.sum').removeClass('hidden');
				$tag.next('.content').addClass('hidden').next('.close-tag').addClass('hidden');
				$(this).removeClass('fa-minus-square');
				$(this).addClass('fa-plus-square');
				$(this).parent().nextAll('.text').addClass('short');
			}
			else if($(this).hasClass('fa-plus-square')) {
				$tag = $(this).parent().parent();
				$(this).parent().nextAll('.sum').addClass('hidden');
				$tag.next('.content').removeClass('hidden');
				$(this).removeClass('fa-plus-square');
				$(this).addClass('fa-minus-square');
				$(this).parent().nextAll('.text').removeClass('short');
			}
		});

		$('.item .fa-plus-square').on('click', function() {
			if($(this).hasClass('fa-minus-square')) {
				console.log('minus');
				$(this).parent().removeClass('item-open');
				$(this).parent().next('.item-list').addClass('hidden');
				$(this).removeClass('fa-minus-square').addClass('fa-plus-square');
			}
			else if($(this).hasClass('fa-plus-square')) {
				console.log('plus');
				$(this).parent().addClass('item-open');
				$(this).parent().next('.item-list').removeClass('hidden');
				$(this).removeClass('fa-plus-square').addClass('fa-minus-square');
			}
		});
		$('.item-where .fa-plus-square').on('click', function() {
			if($(this).hasClass('fa-minus-square')) {
				console.log('minus');
				$(this).parent().next('.item-list').addClass('hidden');
				$(this).removeClass('fa-minus-square').addClass('fa-plus-square');
			}
			else if($(this).hasClass('fa-plus-square')) {
				console.log('plus');
				$(this).parent().next('.item-list').removeClass('hidden');
				$(this).removeClass('fa-plus-square').addClass('fa-minus-square');
			}
		});

		$('.tag-switch').on('click', function() {
			const $tab = $(this).closest('.tab_content');
			let tags = [];
			$('.tag-switch').each(function() {
				if(this.checked) tags.push(this.value);
			});
			console.log(tags);
			$('.item-container', $tab).each(function() {
				let xtags = $(this).data('tags').trim('|').split('|');
				// The item is visible only if has any tag in xtag exists in tags.
				$(this).toggleClass('hidden', !tags.hasIntersection(xtags));
			});
		});


		// Control frame functions

		$('#control-form').each(function() {
			const mainFrame = window.parent.frames['main']; // window.parent.frames['main'];
			console.log("Debug module console is on.");
			setTimeout(function() {
				$('#lasturl', this).val = mainFrame.location.href;
			},500);
			window.setInterval(function() {
				let newurl = mainFrame.location.href;
				const baseUrl = $('#baseurl').val();

				// convert new to relative url
				if(newurl.substr(0,baseUrl.length) === baseUrl) newurl = newurl.substr(baseUrl.length);
				newurl = newurl.replace(/&?debug-session=\w+/, '').replace(/\?$/, '').replace(/\/$/, '');

				let old = window.lastlocation;
				if(old===undefined) old='';
				if(old !== newurl) {
					window.lastlocation = newurl;
					if(old!=='') console.log('checkMain changed: `'+newurl+'` != `' + old + '`');
					// Updates trace window
					const trace = window.parent.parent.frames['trace'];
					trace.location.reload(true);
					// Updates control url
					document.getElementById('control-url').value = newurl;
				}
			}, trace_wait);
		});
		$('#control-form #control-go').on('click', function() {
			let url = $('#control-url').val();
			const a = url.substringAfter('#', false);
			if(url.indexOf('?') === -1) url += '?'; else url += '&';
			url += 'trace=on';
			if(a!=='') url += '#'+a;
			window.top.location = url;
		});

		$('#control-form #control-xmlon').on('click', function() {
			let url = $('#control-url').val();
			const a = url.substringAfter('#', false);
			if(url.indexOf('?') === -1) url += '?'; else url += '&';
			url += 'showxml=on&trace=on';
			if(a!=='') url += '#'+a;
			console.log(url);
			window.top.location = url;
		});
		
		$('#control-form #control-xmloff').on('click', function() {
			let url = $('#control-url').val();
			const a = url.substringAfter('#', false);
			if(url.indexOf('?') === -1) url += '?'; else url += '&';
			url += 'showxml=off&trace=on';
			if(a!=='') url += '#'+a;
			window.top.location = url;
		});
		$('#control-form #control-refreshtrace').on('click', function() {
			window.top.frames['trace'].location.reload(true);
		});
		$('#control-form #control-refreshmain').on('click', function() {
			window.parent.frames['main'].location.reload(true);
			setTimeout(function() {
				window.parent.trace.location.replace('/module/uhi67/uxapp-debug/trace/?session=' + $('#control-session').val());
			}, trace_wait);
		});

		$('.submit').on('change', function () {
			$(this).closest('form').submit();
		})
	});
})(jQuery);

console.log('debug.js');
